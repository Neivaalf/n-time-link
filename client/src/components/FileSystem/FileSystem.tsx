import React, { useState, useEffect } from "react"
import { Redirect } from "react-router-dom"

// import Database from "../Database"
import Toolbar from "../Toolbar"
import PathNavigator from "../PathNavigator"
import Appears from "../Animations/Appears"
import Directory from "../Directory"
import File from "../File"

import styles from "./FileSystem.module.scss"

type item = {
  fullPath: string
  isDirectory: boolean
  isFile: boolean
  name: string
}

type Props = {
  location: Location
}

const getParentDirectory = (path: string): string => {
  const folders = path.split("/")
  folders.pop()
  if (folders.length > 1) return folders.join("/")
  return "/"
}

const FileSystem: React.FC<Props> = ({ location }) => {
  const searchParams = new URLSearchParams(location.search)

  const [items, setItems] = useState<item[]>()
  const [requestedPath, setRequestedPath] = useState<string>(
    searchParams.get("requestedPath") || ""
  )

  // Needed to break the Redirect loop
  useEffect(() => {
    setRequestedPath(searchParams.get("requestedPath") || "")
  }, [searchParams])

  useEffect(() => {
    if (!requestedPath) return
    const getItems = async (_requestedPath: string) => {
      const res = await fetch(
        `/ls?requestedPath=${_requestedPath}` // FIXME: Use proxy to avoid CORS issue
      )
      const body = await res.json()

      setItems(body.stats)
    }

    getItems(requestedPath)
  }, [requestedPath])

  const redirectIfNecessary = (_requestedPath: string) => {
    return _requestedPath === "" ? (
      <Redirect
        to={{
          pathname: "/",
          search: `requestedPath=${process.env.REACT_APP_DEFAULT_FOLDER || "/"}`
        }}
      />
    ) : null
  }

  const showParentIfNecessary = (_requestedPath: string) => {
    return _requestedPath !== "/" ? (
      <Appears>
        <Directory
          item={{
            fullPath: getParentDirectory(_requestedPath),
            isDirectory: true,
            isFile: false,
            name: ".."
          }}
        />
      </Appears>
    ) : null
  }

  const getDirectories = (items: item[]): item[] =>
    items.filter(item => item.isDirectory)

  const getFiles = (items: item[]): item[] => items.filter(item => item.isFile)

  return (
    <div className={styles.FileSystem}>
      {redirectIfNecessary(requestedPath)}
      <Toolbar />
      <div className={styles.explorer}>
        <PathNavigator path={requestedPath} />
        <section className={styles.container}>
          <h2>Directories</h2>
          <div className={styles.directories}>
            {showParentIfNecessary(requestedPath)}
            {items &&
              getDirectories(items).map((item, index) => (
                <Appears
                  key={`${item.name}${item.isDirectory}`}
                  delay={(index + 1) * 5}
                >
                  <Directory item={item} />
                </Appears>
              ))}
          </div>
        </section>
        <section className={styles.container}>
          <h2>Files</h2>
          <div className={styles.files}>
            {items &&
              getFiles(items).map((item, index) => (
                <Appears
                  key={`${item.name}${item.isDirectory}`}
                  delay={index * 5}
                >
                  <File item={item} />
                </Appears>
              ))}
          </div>
        </section>
      </div>
    </div>
  )
}

export default FileSystem
