import React, { useState, useEffect } from "react"

import { ReactComponent as Ifile } from "../../icons/file.svg"

import styles from "./File.module.scss"

type Props = {
  item: {
    fullPath: string
    isDirectory: boolean
    isFile: boolean
    name: string
    links?: link[]
  }
}

type link = {
  id: string
  filePath: string
  numberOfUseLeft: number
}

const File: React.FC<Props> = ({ item }) => {
  const generateLink = async (numberOfUseLeft: number) => {
    const res = await fetch(
      `/generateLink`,
      {
        method: "POST",
        headers: {
          "Content-Type": "application/json"
        },
        body: JSON.stringify({
          filePath: item.fullPath,
          numberOfUseLeft: numberOfUseLeft
        })
      } // FIXME: Use proxy to avoid CORS issue
    )
    const body = await res.json()

    console.log(body)

    setLinks([...links, body])
  }

  const [links, setLinks] = useState<link[]>([])

  const [isOpen, setIsOpen] = useState(false)

  useEffect(() => {
    if (item.links) setLinks(item.links)
  }, [item.links])

  return (
    <div
      className={[styles.File, isOpen ? styles.isOpen : null].join(" ")}
      onClick={() => {
        // generateLink(2)
        setIsOpen(true)
      }}
      title={item.name}
    >
      <Ifile className={styles.icon} /> {item.name}
      {/* <ul onClick={e => e.stopPropagation()}>
        {links.map(link => (
          <li key={link.id}>
            <a href={`/download/${link.id}`}>
              {link.id} ({link.numberOfUseLeft})
            </a>
          </li>
        ))}
      </ul> */}
    </div>
  )
}

export default File
