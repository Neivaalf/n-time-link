import React, { useEffect, useState } from "react"

type link = {
  id: string
  filePath: string
  numberOfUseLeft: number
}

const Database: React.FC = () => {
  const [links, setLinks] = useState<link[]>([])

  useEffect(() => {
    const getLinks = async () => {
      const res = await fetch("/database")
      const body = await res.json()

      setLinks(body)
    }
    getLinks()
  }, [])

  return (
    <div className="Database">
      <h1>Database</h1>
      <pre>{JSON.stringify(links, null, 2)}</pre>
    </div>
  )
}

export default Database
