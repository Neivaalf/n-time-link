import React from "react"
import { Link } from "react-router-dom"

import { ReactComponent as Idirectory } from "../../icons/directory.svg"

import styles from "./Directory.module.scss"

type Props = {
  item: {
    fullPath: string
    isDirectory: boolean
    isFile: boolean
    name: string
  }
}

const Directory: React.FC<Props> = ({ item }) => {
  return (
    <Link
      className={styles.Directory}
      to={{
        pathname: "/",
        search: `requestedPath=${item.fullPath}`
      }}
    >
      <Idirectory className={styles.icon} /> {item.name}
    </Link>
  )
}

export default Directory
