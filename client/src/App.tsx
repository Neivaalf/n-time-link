import React from "react"
import { BrowserRouter, Route } from "react-router-dom"

import FileSystem from "./components/FileSystem"

import styles from "./App.module.scss"

const App: React.FC = () => {
  return (
    <main className={styles.App}>
      <BrowserRouter>
        <Route component={FileSystem} />
      </BrowserRouter>
    </main>
  )
}

export default App
