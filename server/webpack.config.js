const path = require("path")
const webpack = require("webpack")

module.exports = {
  entry: "./src/index.ts",
  mode: "development",
  target: "node",
  module: {
    rules: [
      {
        test: /\.ts$/,
        use: "ts-loader",
        exclude: /node_modules/
      }
    ]
  },
  plugins: [new webpack.DefinePlugin({ "global.GENTLY": false })],
  resolve: {
    extensions: [".ts", ".js"]
  },
  output: {
    path: path.resolve(__dirname, "dist"),
    filename: "server.js"
  },
  externals: [
    {
      formidable: "commonjs formidable"
    }
  ]
}
