import React, { ReactChild } from "react"

import styles from "./Appears.module.scss"

type Props = {
  children: ReactChild
  delay?: number
}

const Appears: React.FC<Props> = ({ children, delay }) => {
  return (
    <div
      style={{ animationDelay: `${delay}ms` }}
      className={[styles.Appears].join(" ")}
    >
      {children}
    </div>
  )
}

export default Appears
