import React, { useState } from "react"
import { Link } from "react-router-dom"

import styles from "./PathNavigator.module.scss"

import { ReactComponent as Ihome } from "../../icons/home.svg"

type Props = {
  path: string
}

type crumb = {
  name: string
  path: string
}

const PathNavigator: React.FC<Props> = ({ path }) => {
  const crumbs = path.split("/")

  const crumbsWithPath: crumb[] = []
  crumbs.forEach((crumb, index) => {
    if (index === 0) {
      crumbsWithPath.push({
        name: crumb,
        path: "/"
      })
    }
    if (index === 1 && crumb !== "") {
      crumbsWithPath.push({
        name: crumb,
        path: `/${crumb}`
      })
    }
    if (index > 1) {
      crumbsWithPath.push({
        name: crumb,
        path: `${crumbsWithPath[index - 1].path}/${crumb}`
      })
    }
  })

  const [indexHovered, setIndexHovered] = useState(-1)

  return (
    <nav className={styles.PathNavigator}>
      {crumbsWithPath.map((crumb, index) => {
        if (crumb.name === "") {
          return (
            <Link
              to={{
                pathname: "/",
                search: `requestedPath=${crumb.path}`
              }}
              title={crumb.path}
              key={crumb.path}
            >
              <Ihome
                className={[
                  styles.icon,
                  styles.crumb,
                  index <= indexHovered ? styles.highlight : null
                ].join(" ")}
              />
            </Link>
          )
        }
        return (
          <Link
            to={{
              pathname: "/",
              search: `requestedPath=${crumb.path}`
            }}
            className={[
              styles.crumb,
              index <= indexHovered ? styles.highlight : null
            ].join(" ")}
            onMouseOver={() => {
              setIndexHovered(index)
            }}
            onMouseOut={() => {
              setIndexHovered(-1)
            }}
            title={crumb.path}
            key={crumb.path}
          >
            {crumb.name}
          </Link>
        )
      })}
    </nav>
  )
}

export default PathNavigator
