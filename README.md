# n-time-link

I want to share files from my dedicated server, but I don't want to share direct links to my filesystem (so I don't have control over who's downloading files, ...)

Run both the server, and client on your server, behind a firewall.

1. Open a SSH tunnel, and port-forward to the client.
2. Browse the filesystem, and choose files you want to share
3. It will generate a key in the local JSON DB with a number of usage left (lowdb)
4. Share the link with other people
5. When other people are looking for a file (with they key), and if there's any use left, it will pass through the matching file and decrement the usage left
   - The server still don't expose the filesystem publicly, only behind the firewall
6. The server return an error if one of the condition is not met

This way I can share files to other people without having to expose my whole filesystem, and without being afraid of them sharing the link since it won't work at some point.

![alt text](image.png)
