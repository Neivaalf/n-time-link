import * as dotenv from "dotenv"

import * as util from "util"
import * as fs from "fs"
import * as path from "path"

import * as Koa from "koa"
import * as Router from "koa-router"
import * as bodyParser from "koa-body"
import * as formidable from "formidable"

import * as lowdb from "lowdb"
import * as FileAsync from "lowdb/adapters/FileAsync"

import * as shortid from "shortid"
import * as _ from "lodash"

type link = {
  id: string
  filePath: string
  numberOfUseLeft: number
}

// Init .env
const result = dotenv.config()

if (result.error) {
  throw result.error
}

// Promisify useful node functions
const pStat = util.promisify(fs.stat)
const pReaddir = util.promisify(fs.readdir)
const pRealpath = util.promisify(fs.realpath)

const start = async () => {
  // Init DB
  const adapter = new FileAsync("./db.json")
  const db = await lowdb(adapter)

  await db
    .defaults({
      links: []
    })
    .write()

  await db.read()

  console.info("Database is ready")

  // Init routing

  // Utils
  const getLinks = async (fullPath: string): Promise<link[]> => {
    await db.read()

    const links = db
      .get("links")
      // @ts-ignore, it works but still need FIXME:
      .filter({
        filePath: fullPath
      })
      .value()

    return links
  }

  const router = new Router()

  router.get("ls", "/ls", async (ctx, next) => {
    const requestedPath = _.get(ctx, "query.requestedPath")
    next()
    if (requestedPath === undefined)
      ctx.throw(400, "requestedPath should not be undefined")

    const stat = await pStat(requestedPath)

    if (!stat.isDirectory())
      ctx.throw(400, "requestedPath should be a Directory")

    const dirents = await pReaddir(requestedPath, {
      withFileTypes: true
    })

    const pStats = dirents.map(async dirent => {
      const realPath = await pRealpath(path.join(requestedPath, dirent.name))
      const stat = await pStat(realPath)
      const fullPath = path.join(requestedPath, dirent.name)

      return {
        fullPath: fullPath,
        name: dirent.name,
        isDirectory: stat.isDirectory(),
        isFile: stat.isFile(),
        links: await getLinks(fullPath)
      }
    })

    const stats = await Promise.all(pStats)

    ctx.body = {
      requestedPath,
      stats
    }
  })

  router.post(
    "generateLink",
    "/generateLink",
    bodyParser(),
    async (ctx, next) => {
      const body = _.get(ctx, "request.body")

      next()

      // TODO: Check if the file exists

      const link: link = {
        id: shortid.generate(),
        filePath: body.filePath,
        numberOfUseLeft: body.numberOfUseLeft
      }

      await db.read()
      await db
        .get("links")
        // @ts-ignore, it works but still need FIXME:
        .push(link)
        .write()

      ctx.body = JSON.stringify(link)
    }
  )

  router.get("database", "/database", async (ctx, next) => {
    next()
    await db.read()
    const links = db.get("links").value()

    ctx.body = links
  })

  router.get("download", "/download/:linkId", async (ctx, next) => {
    const linkId = _.get(ctx, "params.linkId")

    next()

    if (linkId === undefined) ctx.throw(400, "linkId should not be undefined")

    const linkFromDb = await db
      .get("links")
      // @ts-ignore, it works but still need FIXME:
      .find({
        id: linkId
      })
      .value()

    if (linkFromDb === undefined) ctx.throw(404, "linkId not found")

    const numberOfUseLeft = linkFromDb.numberOfUseLeft - 1

    if (numberOfUseLeft > 0) {
      await db
        .get("links")
        // @ts-ignore, it works but still need FIXME:
        .find({
          id: linkId
        })
        .assign({
          numberOfUseLeft: numberOfUseLeft
        })
        .write()
    } else {
      await db
        .get("links")
        // @ts-ignore, it works but still need FIXME:
        .remove({
          id: linkId
        })
        .write()
    }

    ctx.attachment(linkFromDb.filePath)
    ctx.status = 200
    ctx.body = fs.createReadStream(linkFromDb.filePath)
  })

  console.info("Routing is ready")

  // Init Koa
  const app = new Koa()
  app.use(router.routes())

  app.listen(process.env.PORT)
  console.log(`Koa is ready: http://${process.env.HOST}:${process.env.PORT}`)
}

start()
