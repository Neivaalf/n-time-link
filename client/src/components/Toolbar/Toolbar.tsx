import React from "react"

import styles from "./Toolbar.module.scss"

type Props = {}

const Toolbar: React.FC<Props> = () => {
  return (
    <div className={styles.Toolbar}>
      <h1>T</h1>
    </div>
  )
}

export default Toolbar
